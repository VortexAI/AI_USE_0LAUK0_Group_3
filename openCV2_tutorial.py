import cv2
import numpy as np

cap = cv2.VideoCapture(0)

while True:
    ret, frame = cap.read()

    #Color
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    #roi = frame[100:400, 100:400] #Region of Image
    #frame[0:300, 0:300] = roi #Replacing a part of an image

    # Mask the frame: <200 becomes black, >255 becomes white
    ret, mask = cv2.threshold(frame, 200, 255, cv2.THRESH_BINARY)
    ret, otsu = cv2.threshold(gray, 125, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)

    #Gausian adaptive threshholds (book example. Especially for edge detection))
    gaus = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 115, 1)

    #Only taking red items & applying a mask
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV) #First convert to HSV, easier
    lower_red = np.array([10, 150, 150])
    upper_red = np.array([180, 255, 255])
    redMask = cv2.inRange(hsv, lower_red, upper_red)
    red = cv2.bitwise_and(frame, frame, mask = redMask)

    #Noise removal
    kernel = np.ones((15, 15), np.float32) / (15*15)
    redMaskLessNoise = cv2.filter2D(redMask, -1, kernel)
    redLessNoise = cv2.bitwise_and(frame, frame, mask = redMaskLessNoise)

    #Erosion and dilation
    kernel = np.ones((5, 5), np.uint8)
    erosion = cv2.erode(redMask, kernel, iterations = 1) #Make smaller
    dilation = cv2.dilate(redMask, kernel, iterations=1) #Expand

    opening = cv2.morphologyEx(redMask, cv2.MORPH_OPEN, kernel) #Remove false positives
    closing = cv2.morphologyEx(redMask, cv2.MORPH_CLOSE, kernel)  # Remove false positives

    #Gradients
    laplacian = cv2.Laplacian(gray, cv2.CV_64F)
    sobelx = cv2.Sobel(frame, cv2.CV_64F, 1, 0, ksize = 5)
    sobely = cv2.Sobel(frame, cv2.CV_64F, 0, 1, ksize=5)

    #Edge detection
    edges = cv2.Canny(frame, 100, 150)


    cv2.imshow('frame', frame)
    cv2.imshow('gray', gray)
    #cv2.imshow('roi', roi)
    cv2.imshow('mask', mask)
    cv2.imshow('gaus', gaus)
    cv2.imshow('otsu', otsu)
    cv2.imshow('redMask', redMask)
    cv2.imshow('red', red)
    cv2.imshow('redMaskLessNoise', redMaskLessNoise)
    cv2.imshow('redLessNoise', redLessNoise)
    cv2.imshow('erosion', erosion)
    cv2.imshow('dilation', dilation)
    cv2.imshow('opening', opening)
    cv2.imshow('closing', closing)
    cv2.imshow('laplacian', laplacian)
    cv2.imshow('sobelx', sobelx)
    cv2.imshow('sobely', sobely)
    cv2.imshow('edges', edges)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()