print("hello world!!!")

class Instance(object):

    def __init__(self, label_id, top_y, top_x, bottom_y, bottom_x, last_seen, camera_id, photo, detection_certainty):
        self.id = -1
        self.label_id = label_id
        self.top_y = top_y
        self.top_x = top_x
        self.bottom_y = bottom_y
        self.bottom_x = bottom_x
        self.last_seen = last_seen
        self.camera_id = camera_id
        self.photo = photo
        self.detection_certainty = detection_certainty
        self.matched = False
        self.times_detected = 0
        self.remove = False
        self.best_photo = photo
        self.best_photo_score = detection_certainty
        self.to_be_matched = True
        self.first_seen = last_seen
        self.keypoints = None
        self.descriptors = None
        self.kpdes_calculated = False
