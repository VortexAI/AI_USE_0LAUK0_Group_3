import os
import sys
import tarfile
import cv2

import numpy as np
import six.moves.urllib as urllib
import tensorflow as tf
import matplotlib; matplotlib.use('Agg')  # pylint: disable=multiple-statements
from pathlib import Path
from PIL import Image
#from matplotlib import pyplot as plt

cap = cv2.VideoCapture(0)
models_dir = "C:\\Eigen Domein\\,UNI\\0LAUK0 USE Project\\models\\research"
# This is needed since the notebook is stored in the object_detection folder.
sys.path.append(models_dir)
from object_detection.utils import ops as utils_ops
from object_detection.utils import label_map_util

from object_detection.utils import visualization_utils as vis_util

if tf.__version__ < '1.4.0':
    raise ImportError('Please upgrade your tensorflow installation to v1.4.* or later!')

# What model to download.
MODEL_NAME = 'ssd_mobilenet_v1_coco_2017_11_17'
MODEL_FILE = MODEL_NAME + '.tar.gz'
DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'

# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'

# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = os.path.join(models_dir + '/object_detection/data', 'mscoco_label_map.pbtxt')

NUM_CLASSES = 90

MODEL_PATH = Path(MODEL_NAME)

if not MODEL_PATH.is_dir():
    opener = urllib.request.URLopener()
    opener.retrieve(DOWNLOAD_BASE + MODEL_FILE, MODEL_FILE)
    tar_file = tarfile.open(MODEL_FILE)
    for file in tar_file.getmembers():
        file_name = os.path.basename(file.name)
        if 'frozen_inference_graph.pb' in file_name:
            tar_file.extract(file, os.getcwd())

detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES,
                                                            use_display_name=True)
category_index = label_map_util.create_category_index(categories)


def load_image_into_numpy_array(image):
    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape(
        (im_height, im_width, 3)).astype(np.uint8)

def run_inference_for_single_image(image, graph, sess):

    # Get handles to input and output tensors
    ops = tf.get_default_graph().get_operations()
    all_tensor_names = {output.name for op in ops for output in op.outputs}
    tensor_dict = {}
    for key in [
        'num_detections', 'detection_boxes', 'detection_scores',
        'detection_classes', 'detection_masks'
    ]:
        tensor_name = key + ':0'
        if tensor_name in all_tensor_names:
            tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(
                tensor_name)
    if 'detection_masks' in tensor_dict:
        # The following processing is only for single image
        detection_boxes = tf.squeeze(tensor_dict['detection_boxes'], [0])
        detection_masks = tf.squeeze(tensor_dict['detection_masks'], [0])
        # Reframe is required to translate mask from box coordinates to image coordinates and fit the image size.
        real_num_detection = tf.cast(tensor_dict['num_detections'][0], tf.int32)
        detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
        detection_masks = tf.slice(detection_masks, [0, 0, 0], [real_num_detection, -1, -1])
        detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
            detection_masks, detection_boxes, image.shape[0], image.shape[1])
        detection_masks_reframed = tf.cast(
            tf.greater(detection_masks_reframed, 0.5), tf.uint8)
        # Follow the convention by adding back the batch dimension
        tensor_dict['detection_masks'] = tf.expand_dims(
            detection_masks_reframed, 0)
    image_tensor = tf.get_default_graph().get_tensor_by_name('image_tensor:0')

    # Run inference
    output_dict = sess.run(tensor_dict,
                           feed_dict={image_tensor: np.expand_dims(image, 0)})

    # all outputs are float32 numpy arrays, so convert types as appropriate
    output_dict['num_detections'] = int(output_dict['num_detections'][0])
    output_dict['detection_classes'] = output_dict[
        'detection_classes'][0].astype(np.uint8)
    output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
    output_dict['detection_scores'] = output_dict['detection_scores'][0]
    if 'detection_masks' in output_dict:
        output_dict['detection_masks'] = output_dict['detection_masks'][0]
    return output_dict


surf_img = cv2.imread('images/surf-target.jpg', 0)
bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

sift = cv2.xfeatures2d.SIFT_create()
kp1, des1 = sift.detectAndCompute(surf_img, None)

#fast = cv2.FastFeatureDetector_create()
#kp1 = fast.detect(surf_img, None)

#surf = cv2.xfeatures2d.SIFT_create()
#kp1, des1 = surf.detectAndCompute(surf_img, None)

index_params = dict(algorithm=0, trees=5)
search_params = dict(checks=50)
flann = cv2.FlannBasedMatcher(index_params, search_params)

MIN_MATCH_COUNT = 15

with detection_graph.as_default() as graph:
    with tf.Session() as sess:
        while True:
            ret, image_np = cap.read()
            if True:
                kp2, des2 = sift.detectAndCompute(image_np, None)
                #kp2, des2 = fast.detect(image_np, None)
                #kp2, des2 = surf.detectAndCompute(image_np, None)
                #matches = bf.knnMatch(des1, des2, k=2)
                matches = flann.knnMatch(des1, des2, k=2)
                #print(matches[0].distance)
                #print(matches[len(matches)-1].distance)
                good = []
                for m,n in matches:
                    if m.distance < 0.7*n.distance:
                        good.append(m)

                image_out = None

                if len(good) > MIN_MATCH_COUNT:
                    src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1,1,2)
                    dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1,1,2)

                    M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
                    matchesMask = mask.ravel().tolist()

                    h,w = surf_img.shape
                    pts = np.float32([[0, 0], [0, h - 1], [w - 1, h - 1], [w - 1, 0]]).reshape(-1, 1, 2)
                    dst = cv2.perspectiveTransform(pts, M)

                    img2 = cv2.polylines(image_np, [np.int32(dst)], True, 255, 3, cv2.LINE_AA)

                    draw_params = dict(matchColor=(0, 255, 0),  # draw matches in green color
                                       singlePointColor=None,
                                       matchesMask=matchesMask,  # draw only inliers
                                       flags=2)

                    image_out = cv2.drawMatches(surf_img, kp1, image_np, kp2, good, None, **draw_params)
                else:
                    image_out = cv2.drawMatches(surf_img, kp1, image_np, kp2, [], None, flags=2)

            # Actual detection.
            image_border = cv2.copyMakeBorder(cv2.resize(image_np, (800, 600)), 0, 0, 800, 0, cv2.BORDER_CONSTANT)
            output_dict = run_inference_for_single_image(image_border, graph, sess)
            # Visualization of the results of a detection.

            if True:
                vis_util.visualize_boxes_and_labels_on_image_array(
                    image_out,
                    output_dict['detection_boxes'],
                    output_dict['detection_classes'],
                    output_dict['detection_scores'],
                    category_index,
                    instance_masks=output_dict.get('detection_masks'),
                    use_normalized_coordinates=True,
                    line_thickness=8,
                    min_score_thresh=0.3)

            cv2.imshow('object detection', cv2.resize(image_out, (1600, 600)))
            if cv2.waitKey(25) & 0xFF == ord('q'):
                cv2.destroyAllWindows()
                break
