import os
import sys
import tarfile
import cv2
import MySQLdb
import requests

import time
import math
import numpy as np
import six.moves.urllib as urllib
import tensorflow as tf
import matplotlib
import _thread
matplotlib.use('Agg')  # pylint: disable=multiple-statements

from pathlib import Path

from Classes import *
from settings import *

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append(MODELS_DIR)

from object_detection.utils import ops as utils_ops
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util

# --------------- SETTINGS ----------------
UPDATE_INTERVAL = 10  # Interval at which updates are pushed to the database
ENABLE_VISUAL_OUTPUT = True  # Whether you want to see the live feed screen or not
CAMERA_ID_OFFSET = 0               # What value to add to the number of this camera
IMAGE_API_URL = "https://ol.pietervoors.com/api/instance/{}/image"     # URL of the Image API
OBJECT_CERTAINTY_LOWER_THRESHOLD = 0.4      # The minimal needed certainty to assume an instance is another one
OBJECT_DETECTION_THRESHOLD = 0.5            # Minimal needed certain to classify new object
OBJECT_DETECTION_MATCHING_THRESHOLD = 0.3   # Minimal needed certainty to classify object to see if it matches existing one
PROBABILITY_POSDIF_SCALAR = 0.02            # Scalar for probability based on positional difference. Lower = must be closer together to be sure
PROBABLLITY_POSDIF_POWER = 6                # Higher = graph for posdif certainty more linear
MATCHING_TIME_THRESHOLD = 3.0               # Time after which only histogram matching is to be done
MATCHING_HISTOGRAM_POWER = 5                # Scaling for histogram. Higher = quicker uncertainty of match
OBJECT_CERTAINTY_LONGER_TIME_POSDIF_MIN = 0.6   # Minimal value to declare positional equal object if longer time unseen
OBJECT_CERTAINTY_LONGER_TIME_HIST_MIN = 0.5     # Minimal value to declare hist equal if longer time unseen
OBJECT_CERTAINTY_LONGER_TIME_FEATURES_MIN = 0.5     # Minimal value to declare features equal if longer time unseen

MODEL_NAME = 'faster_rcnn_inception_v2_coco_2018_01_28'     # Name of frozen inference graph from Tensorflow model zoo

TIMEDIF_CERTAINTY_BONUS_THRESHOLD = 1000    # The amount of miliseconds under which the certainty should get a boost
TIMEDIF_CERTAINTY_BONUS_SCALAR = 1.5        # The value the certainty should be scaled with if minimal time dif
TIMEDIF_CERTAINTY_MIN_TIME = 200            # The amount of miliseconds under which the full scalar should be applied
TIMEDIF_CERTAINTY_BONUS_ARC_BEND = 2        # Determines the curve of the time bonus function. Higher means longer high scaling

MIN_TIME_BEFORE_PUSH = 5.0                  # Seconds that an item needs to have been detected to be pushed
MIN_TIMES_DETECTED = 3                      # Number of times that an item must have been detected before pushing

OVERLAP_EXTENDED_PERCENTAGE = 0.4           # Which percentage of a smaller box may be outside the bigger box on one side to still be considered inside
DEBUG_LABEL_ID = -1                         # The label ID to display extended debug messages for

# Label IDs of the items to include in detection set
LABEL_ID_WHITELIST = {1, 27, 28, 31, 33, 37, 44, 46, 47, 48, 49, 50, 51, 62, 63, 64, 67, 73, 74, 75, 76, 77, 84, 85, 86, 87, 88, 89, 90}
# -----------------------------------------

# Open webcam stream
caps = []
for webcam_id in WEBCAM_IDS:
    caps.append(cv2.VideoCapture(webcam_id))

print("Starting execution at time " + str(time.time()))

if tf.__version__ < '1.4.0':
    raise ImportError('Please upgrade your tensorflow installation to v1.4.* or later!')

# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'

# List of the strings that is used to add correct label for each box. NB: Using COCO Dataset
PATH_TO_LABELS = os.path.join(MODELS_DIR + '/object_detection/data', 'mscoco_label_map.pbtxt')
NUM_CLASSES = 90
MODEL_PATH = Path(MODEL_NAME)

# Download graph if not already downloaded
if not MODEL_PATH.is_dir():
    print("Downloading model "+str(MODEL_NAME))
    MODEL_FILE = MODEL_NAME + '.tar.gz'
    DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'
    opener = urllib.request.URLopener()
    opener.retrieve(DOWNLOAD_BASE + MODEL_FILE, MODEL_FILE)
    tar_file = tarfile.open(MODEL_FILE)
    for file in tar_file.getmembers():
        file_name = os.path.basename(file.name)
        if 'frozen_inference_graph.pb' in file_name:
            tar_file.extract(file, os.getcwd())

# Loading in the Neural Network Graph
detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

# Loading in the labels
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES,
                                                            use_display_name=True)
category_index = label_map_util.create_category_index(categories)


def load_image_into_numpy_array(image):
    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape(
        (im_height, im_width, 3)).astype(np.uint8)


def run_inference_for_single_image(image, graph, sess):
    # Get handles to input and output tensors
    ops = tf.get_default_graph().get_operations()
    all_tensor_names = {output.name for op in ops for output in op.outputs}
    tensor_dict = {}
    for key in [
        'num_detections', 'detection_boxes', 'detection_scores',
        'detection_classes', 'detection_masks'
    ]:
        tensor_name = key + ':0'
        if tensor_name in all_tensor_names:
            tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(
                tensor_name)

    if 'detection_masks' in tensor_dict:
        # The following processing is only for single image
        detection_boxes = tf.squeeze(tensor_dict['detection_boxes'], [0])
        detection_masks = tf.squeeze(tensor_dict['detection_masks'], [0])   # Only used if mask trained model
        # Reframe is required to translate mask from box coordinates to image coordinates and fit the image size.
        real_num_detection = tf.cast(tensor_dict['num_detections'][0], tf.int32)
        detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
        detection_masks = tf.slice(detection_masks, [0, 0, 0], [real_num_detection, -1, -1])
        detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
            detection_masks, detection_boxes, image.shape[0], image.shape[1])
        detection_masks_reframed = tf.cast(
            tf.greater(detection_masks_reframed, 0.5), tf.uint8)
        # Follow the convention by adding back the batch dimension
        tensor_dict['detection_masks'] = tf.expand_dims(
            detection_masks_reframed, 0)
    image_tensor = tf.get_default_graph().get_tensor_by_name('image_tensor:0')

    # Run inference
    output_dict = sess.run(tensor_dict,
                           feed_dict={image_tensor: np.expand_dims(image, 0)})

    # all outputs are float32 numpy arrays, so convert types as appropriate
    output_dict['num_detections'] = int(output_dict['num_detections'][0])
    output_dict['detection_classes'] = output_dict[
        'detection_classes'][0].astype(np.uint8)
    output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
    output_dict['detection_scores'] = output_dict['detection_scores'][0]
    if 'detection_masks' in output_dict:
        output_dict['detection_masks'] = output_dict['detection_masks'][0]
    return output_dict


# Execute array of queries
def execute_queries(queries):
    for query in queries:
        dbCur.execute(query)


# Compare how much two instances are alike, returns [0.0, 1.0]
# Unlike expected, returns 0.0 for comparison of two references to the same instance
# NB: includeOldInstances to True matches only to older instances that are not still-to-be-matched
def compare_instances(object1: Instance, object2: Instance, matchInstances=True):
    label = object1.label_id

    # Sanity check whether these instances are even of the same object
    if object1.label_id != object2.label_id:
        if label == DEBUG_LABEL_ID:
            print("     Items are not the same type of object (return 0)")
        return 0.0

    # If object is already matched to different detection this frame, then skip
    if object1.matched or object2.matched:
        if label == DEBUG_LABEL_ID:
            print("     One of these items has already been matched this frame (return 0)")
        return 0.0

    # Two references to the same object, something went wrong, lets not confuse max value detection
    if object1 is object2:
        if label == DEBUG_LABEL_ID:
            print("     Comparing item against itself, something is going wrong (return 0)")
        return 0.0

    if matchInstances and object1.to_be_matched and object2.to_be_matched:
        # Trying to match item, but items have both not attempted to match to global so don't match together
        # I.o.w.: Both are still in their temporary tracking before a decision is made, so don't say they are the same
        if label == DEBUG_LABEL_ID:
            print("     Trying to match, but both items are yet to be matched, so not matching them together (return 0)")
        return 0.0

    time_difference = abs(object1.last_seen - object2.last_seen)
    if time_difference > MATCHING_TIME_THRESHOLD and not matchInstances:
        if label == DEBUG_LABEL_ID:
            print("     Time difference too big to compare without considering matching (return 0)")
        return 0.0

    if time_difference > MATCHING_TIME_THRESHOLD:
        # This item hasn't been detected for a while:
        #   Make the comparison depend more on picture similarity than location

        compare_certainty_location = compare_instances_location_and_size(object1, object2)
        compare_certainty_hist = compare_instances_histogram(object1, object2)
        compare_certainty_features = compare_instances_features(object1, object2)
        if label == DEBUG_LABEL_ID:
            print("     Not seen for a while. Scores[location: "+str(compare_certainty_location)+", hist: "
                  +str(compare_certainty_hist)+", features: "+str(compare_certainty_features)+"]")
        if (compare_certainty_location >= OBJECT_CERTAINTY_LONGER_TIME_POSDIF_MIN
                and compare_certainty_hist >= OBJECT_CERTAINTY_LONGER_TIME_HIST_MIN
                and compare_certainty_features >= OBJECT_CERTAINTY_LONGER_TIME_FEATURES_MIN):
            compare_certainty = compare_certainty_location
            if label == DEBUG_LABEL_ID:
                print("         This suffices to do location matching (return the location score)")
        else:
            compare_certainty = (compare_certainty_hist + compare_certainty_features) / 2
            if label == DEBUG_LABEL_ID:
                print("         This does not suffice to do location matching, so feature&hist combined into score: "
                      +str(compare_certainty))
    else:
        # This item has recently been detected, compare with heavy use of position & scale
        compare_certainty = compare_instances_location_and_size(object1, object2)
        if label == DEBUG_LABEL_ID:
            print("     Since other item was recently seen, compare score on location&size: "+str(compare_certainty))
    return compare_certainty


# Score of equality based on point feature matching
def compare_instances_features(object1: Instance, object2: Instance):
    # Calculate keypoints and descriptors (basically, the features) of both instances if not done yet
    if not object1.kpdes_calculated:
        object1.keypoints, object1.descriptors = sift.detectAndCompute(object1.best_photo, None)
        object1.kpdes_calculated = True
    if not object2.kpdes_calculated:
        object2.keypoints, object2.descriptors = sift.detectAndCompute(object2.best_photo, None)
        object2.kpdes_calculated = True

    # Execute point feature matching
    matches = flann_matcher.knnMatch(np.float32(object1.descriptors), np.float32(object2.descriptors), k=2)

    # Extract these matches into a score
    score = 0
    for m, n in matches:
        if m.distance < 0.7 * n.distance:
            score += 1

    # Map the score to a probability [0.0, 1.0]
    feature_certainty = max(0.0, min(1.0, 1 - pow(1 / score, 1/5)))

    return feature_certainty


# Compare how much 2 images are alike based on the color space
def compare_instances_histogram(object1: Instance, object2: Instance):
    # Compare color histogram
    # TODO Optimize to only calculate histogram over image once
    img1 = cv2.cvtColor(object1.best_photo, cv2.COLOR_BGR2HSV)
    img2 = cv2.cvtColor(object2.best_photo, cv2.COLOR_BGR2HSV)
    hist1 = cv2.calcHist([img1], [0, 1], None, [180, 256],
                         [0, 180, 0, 256])  # https://docs.opencv.org/3.1.0/dd/d0d/tutorial_py_2d_histogram.html
    hist2 = cv2.calcHist([img2], [0, 1], None, [180, 256], [0, 180, 0, 256])
    probability_hist = pow(cv2.compareHist(hist1, hist2, cv2.HISTCMP_CORREL), MATCHING_HISTOGRAM_POWER)
    return probability_hist


def check_object_1_in_2(object1: Instance, object2: Instance):
    extend_x = (object1.bottom_x - object1.top_x) * OVERLAP_EXTENDED_PERCENTAGE
    extend_y = (object1.bottom_y - object1.top_y) * OVERLAP_EXTENDED_PERCENTAGE

    if (object1.top_x + extend_x > object2.top_x and object1.top_y + extend_y > object2.top_y and
           object1.bottom_x < object2.bottom_x and object1.bottom_y < object2.bottom_y):
        return True     # Slightly over top left
    elif (object1.top_x > object2.top_x and object1.top_y + extend_y > object2.top_y and
           object1.bottom_x - extend_x < object2.bottom_x and object1.bottom_y < object2.bottom_y):
        return True     # Slightly over top right
    elif (object1.top_x > object2.top_x and object1.top_y > object2.top_y and
           object1.bottom_x - extend_x < object2.bottom_x and object1.bottom_y - extend_y < object2.bottom_y):
        return True     # Slgihtly over bottom right
    elif (object1.top_x + extend_x > object2.top_x and object1.top_y > object2.top_y and
           object1.bottom_x < object2.bottom_x and object1.bottom_y - extend_y < object2.bottom_y):
        return True     # Slightly over bottom left
    else:
        return False    # Not overlapping


# Probabilty two objects are equal based on location and size
def compare_instances_location_and_size(object1: Instance, object2: Instance):
    # Check if they are even in the same camera
    if object1.camera_id is not object2.camera_id:
        return 0.0

    # Find centers and sizes
    center1_x = (object1.bottom_x + object1.top_x) / 2
    center1_y = (object1.bottom_y + object1.top_y) / 2
    size1_x = object1.bottom_x - object1.top_x
    size1_y = object1.bottom_y - object1.top_y
    size1 = size1_x * size1_y
    center2_x = (object2.bottom_x + object2.top_x) / 2
    center2_y = (object2.bottom_y + object2.top_y) / 2
    size2_x = object2.bottom_x - object2.top_x
    size2_y = object2.bottom_y - object2.top_y
    size2 = size2_x * size2_y

    # Find the difference in the axis relative to the average size
    pos_dif_x = abs(center1_x - center2_x) / ((size1_x + size2_x) / 2)
    pos_dif_y = abs(center1_y - center2_y) / ((size1_y + size2_y) / 2)
    pos_dif = math.sqrt(pow(pos_dif_x, 2) + pow(pos_dif_y, 2))      # Pythagoras for dist over xdist ydist

    # Make the distance into a probability for the positional difference
    if pos_dif == 0.0:
        probability_position = 1
    else:
        probability_position = min(1.0, pow(PROBABILITY_POSDIF_SCALAR / pos_dif, 1.0 / PROBABLLITY_POSDIF_POWER))
        # print("pos_dif: "+str(pos_dif))

    # Calculate probability with regards to how equal items are in size
    probability_size = min(size1, size2) / max(size1, size2)

    # Combine the probabilities
    calculated_certainty = (probability_position + probability_size) / 2

    # TODO Scale the probability by time?
    # if time_difference < TIMEDIF_CERTAINTY_BONUS_THRESHOLD:
    #     calculated_scalar = TIMEDIF_CERTAINTY_BONUS_SCALAR
    #     if time_difference > TIMEDIF_CERTAINTY_MIN_TIME:
    #         scalar = TIMEDIF_CERTAINTY_BONUS_SCALAR
    #         min_time = TIMEDIF_CERTAINTY_MIN_TIME
    #         threshold = TIMEDIF_CERTAINTY_BONUS_THRESHOLD
    #         power = TIMEDIF_CERTAINTY_BONUS_ARC_BEND
    #         calculated_scalar = 1 + (scalar - 1) * (1 - pow((time_difference - min_time) / (threshold - min_time), power))
    #     calculated_certainty += min(1-calculated_certainty, calculated_certainty, (calculated_certainty - 0.5))
    #         # 1 - ((1 - calculated_certainty) / calculated_scalar)

    return calculated_certainty


# Push all new info and pictures of instances to the database
# Supports async running
def update_database(last_updated_time_async):
    print("pushing time")

    # Loop over each item we have tracked
    label_ids = set(data.keys())
    for label_id_async in label_ids:
        instances = data.get(label_id_async).copy()
        for instance_async in instances:

            # If the instance was last updated before last push, do not push to DB again
            if instance_async.last_seen < last_updated_time_async - 3.0:
                continue

            # This detection still needs to be checked if it corresponds to existing
            if instance_async.to_be_matched:
                continue

            if instance_async.id >= 0:
                # Construct query
                # print("Updating label " + str(label_id) + " instance id " + str(instance.id))
                query = "UPDATE instances SET "
                query += "top_y = " + str(instance_async.top_y)
                query += ", top_x = " + str(instance_async.top_x)
                query += ", bottom_y = " + str(instance_async.bottom_y)
                query += ", bottom_x = " + str(instance_async.bottom_x)
                query += ", camera_id = " + str(instance_async.camera_id)
                query += ", updated_at = FROM_UNIXTIME(" + str(instance_async.last_seen) + ")"
                query += " WHERE id = " + str(instance_async.id) + ";"
                # print(query)
                dbCur.execute(query)

            else:
                # print("Inserting new instance of label " + str(label_id))
                query = "INSERT INTO instances "
                query += "(label_id, camera_id, top_y, top_x, bottom_y, bottom_x, created_at, updated_at) "
                query += "VALUES (" + str(label_id_async) + ", "
                query += str(instance_async.camera_id) + ", "
                query += str(instance_async.top_y) + ", "
                query += str(instance_async.top_x) + ", "
                query += str(instance_async.bottom_y) + ", "
                query += str(instance_async.bottom_x) + ", "
                query += "FROM_UNIXTIME(" + str(instance_async.last_seen) + "), "
                query += "FROM_UNIXTIME(" + str(instance_async.last_seen) + "));"
                # print(query)

                dbCur.execute(query)
                instance_async.id = dbCur.lastrowid

            # Writing and uploading image
            cv2.imwrite("db_images/item_" + str(instance_async.id) + ".png", instance_async.photo)  # Write image to disk
            image = {'img': open("db_images/item_" + str(instance_async.id) + ".png", 'rb')}
            headers = {"Accept": "application/json", "Authorization": "Bearer "+IMAGE_API_KEY}
            response = requests.post(IMAGE_API_URL.format(instance_async.id), files=image, headers=headers)     # Upload
            print("Uploaded item ID "+str(instance_async.id)+" of class "+str(label_id_async)+". Response: " + str(response.text))


# Execute object detetion and call matching on single camera
def process_cam(cam_id):
    cap = caps[cam_id]

    # Actual detection.
    ret, image_np = cap.read()
    width, height = image_np.shape[:2]
    output_dict = run_inference_for_single_image(image_np, graph, sess)

    # Filtering out the good scoring detections
    detection_boxes = output_dict['detection_boxes']
    detection_classes = output_dict['detection_classes']
    detection_scores = output_dict['detection_scores']
    for i in range(0, len(detection_boxes)):
        label_id = detection_classes[i]

        # If we do not want to detect this item, make sure it doesn't show and don't analyze it
        if label_id not in LABEL_ID_WHITELIST:
            detection_scores[i] = 0
            continue

        if detection_scores[i] > OBJECT_DETECTION_MATCHING_THRESHOLD:
            # print("boxes: "+str(detection_boxes[i]))
            # print("classes: "+str(detection_classes[i]))
            # print("scores: "+str(detection_scores[i]))

            new_instance = True  # To keep track of whether we find which instance this is or if it's a new one
            instance = Instance(label_id,  # First make a new instance with variables found
                                detection_boxes[i][0], detection_boxes[i][1], detection_boxes[i][2],
                                detection_boxes[i][3], time.time(), cam_id+CAMERA_ID_OFFSET, image_np.copy(),
                                detection_scores[i])
            compare_instance = instance  # Will hold the instance that we think it is
            certainty = OBJECT_CERTAINTY_LOWER_THRESHOLD  # Holds the certainty that it is that instance

            # Check each other instance of this object to see if this one is the same, first only recently seen
            for comparing_instance in data.get(label_id, set()):
                compare_certainty = compare_instances(instance, comparing_instance, False)

                # If we are more sure about this one than the current one, replace it
                if compare_certainty > certainty:
                    certainty = compare_certainty
                    compare_instance = comparing_instance
                    new_instance = False

            if compare_instance.to_be_matched:
                if (compare_instance.times_detected >= MIN_TIMES_DETECTED and
                        (compare_instance.last_seen - compare_instance.first_seen) >= MIN_TIME_BEFORE_PUSH):

                    original_instance = compare_instance

                    if label_id == DEBUG_LABEL_ID:
                        print("Going to do matching on item of class " +
                              str(label_id) + " (own ID: "+str(original_instance.id)+")")

                    # Set back certainty to threshold: It was used to see which to-be-matched instance it is,
                    #   but now we want to match again, but to older instances to see if it is one
                    certainty = OBJECT_CERTAINTY_LOWER_THRESHOLD

                    # Compare against each instance to see if this is the other one. If not, this is a new entry
                    for comparing_instance in data.get(label_id, set()):

                        if label_id == DEBUG_LABEL_ID:
                            print("Comparing against ID "+str(comparing_instance.id))

                        # Check against each other instance for overlap, if so, delete this item
                        if check_object_1_in_2(original_instance, comparing_instance) and not (comparing_instance.to_be_matched):
                            if label_id == DEBUG_LABEL_ID:
                                print(" -It's inside this one, need to delete "+str(original_instance.id))
                            compare_instance = comparing_instance
                            break

                        compare_certainty = compare_instances(original_instance, comparing_instance, True)

                        if label_id == DEBUG_LABEL_ID:
                            print(" -Tested. Certainty: " + str(compare_certainty) + " to compete against current " + str(certainty))

                        # If we are more sure about this one than the current one, replace it
                        if compare_certainty > certainty:
                            # print("Which is higher than old value!")
                            certainty = compare_certainty
                            compare_instance = comparing_instance
                            new_instance = False

                    if original_instance is not compare_instance:
                        if label_id == DEBUG_LABEL_ID:
                            print("     This item has matched to "+str(compare_instance.id)+" so delete caller (= merge the two since they are the same instance)")
                        compare_instance.times_detected += original_instance.times_detected
                        data.get(label_id).discard(original_instance)

                    compare_instance.to_be_matched = False

            # Update info to the data array
            if new_instance:
                # If we DID NOT find an existing instance, add a new one
                if detection_scores[i] > OBJECT_DETECTION_THRESHOLD:
                    # IF new object, it needs to be pretty sure it is such object
                    temp = data.get(label_id, set())
                    temp.add(compare_instance)
                    data[label_id] = temp
            else:
                # If we DID find an existing instance, update its variables
                compare_instance.matched = True
                compare_instance.top_y = detection_boxes[i][0]
                compare_instance.top_x = detection_boxes[i][1]
                compare_instance.bottom_y = detection_boxes[i][2]
                compare_instance.bottom_x = detection_boxes[i][3]
                compare_instance.last_seen = time.time()
                compare_instance.camera_id = cam_id + CAMERA_ID_OFFSET
                compare_instance.photo = image_np.copy()
                compare_instance.detection_certainty = detection_scores[i]
                compare_instance.times_detected += 1

                if detection_scores[i] > compare_instance.best_photo_score:
                    compare_instance.best_photo_score = detection_scores[i]
                    compare_instance.best_photo = image_np[
                                                  int(width*compare_instance.top_x):int(height*compare_instance.top_y),
                                                  int(width*compare_instance.bottom_x):int(height*compare_instance.bottom_y)].copy()
                    compare_instance.kpdes_calculated = False

                detection_scores[i] = 1.0 + certainty  # Temp set visible label, to see when seen as other object

    # Visualization of the results of a detection.
    if ENABLE_VISUAL_OUTPUT:
        vis_util.visualize_boxes_and_labels_on_image_array(
            image_np,
            output_dict['detection_boxes'],
            output_dict['detection_classes'],
            output_dict['detection_scores'],
            category_index,
            instance_masks=output_dict.get('detection_masks'),
            use_normalized_coordinates=True,
            line_thickness=8,
            min_score_thresh=0.3)
        cv2.imshow("Camera " + str(cam_id), image_np)


# ---------------- Main -----------------------

# Set up connection to database
db = MySQLdb.connect(host=DB_HOST,  # your host, usually localhost
                     user=DB_USER,  # your username
                     passwd=DB_PASSWD,  # your password
                     db=DB_DB)  # name of the data base
db.autocommit(True)     # Auto commit every time the local copy of the DB is updated
dbCur = db.cursor(MySQLdb.cursors.DictCursor)  # Cursor object lets you execute MySQL queries

# Initializations
sift = cv2.xfeatures2d.SIFT_create()
index_params = dict(algorithm=0, trees=5)
search_params = dict(checks=50)
flann_matcher = cv2.FlannBasedMatcher(index_params, search_params)

lastUpdatedTime = time.time()
lastFrameTime = time.time()
data = dict()

frame = 0

with detection_graph.as_default() as graph:
    with tf.Session() as sess:
        while True:

            print("Frame "+str(frame))
            frame += 1

            # Check for update to database
            if time.time() >= lastUpdatedTime + UPDATE_INTERVAL:
                preUpdateTime = time.time()
                _thread.start_new_thread(update_database, (lastUpdatedTime,))
                # update_database(lastUpdatedTime)
                lastUpdatedTime = preUpdateTime

            # Mark each instance as not matched yet. TODO Can optimize this since it now resets items that were never changed
            for label_id, object_class in data.items():
                remove = set()
                for instance in object_class:
                    instance.matched = False

                    # Discard items that are too long not seen and not seen many times
                    if instance.last_seen < time.time() - MIN_TIME_BEFORE_PUSH:
                        if instance.times_detected < MIN_TIMES_DETECTED:
                            remove.add(instance)

                for instance in remove:
                    object_class.discard(instance)

            # Read and analyze each camera
            for cam_id in range(0, len(caps)):
                process_cam(cam_id)

            #   Test error for threading without locks
            # threads = []
            # for cam_id in range(0, len(caps)):
            #         t = Thread(target=process_cam, args=(cam_id,))
            #         t.start()
            #         threads.append(t)
            # for thread in threads:
            #     thread.join()

            if cv2.waitKey(25) & 0xFF == ord('q'):
                cv2.destroyAllWindows()
                break

            # Update last frame time
            lastFrameTime = time.time()

db.close()
